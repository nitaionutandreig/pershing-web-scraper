from datetime import datetime
import pyPdf

def check_date(date):
    if date == '3Q 2014':
        return datetime.strptime('01 January 2014', '%d %B %Y')
    else:
        return datetime.strptime(date, '%d %B %Y')

def valid_date(date):
    try:
        return datetime.strptime(date, "%Y%m%d")
    except ValueError:
        print "Not a valid date: '{0}'.".format(date)

def extract_text(download_url, path):
    content = ""
    filename = download_url.split('/')[-1]
    pdf = pyPdf.PdfFileReader(open(path + '/' + filename, 'rb'))
    for i in range(0, pdf.getNumPages()):
        content += pdf.getPage(i).extractText() + "\n"
    f = open(path + '/' + filename.split('.')[0] + '.txt', 'w')
    f.write(content.encode('UTF-8'))
    f.close()
    print 'Text extracted from {}'.format(filename)
    
