import os
import argparse
import requests
from selenium import webdriver
from datetime import datetime
import csv
from utils import check_date, valid_date, extract_text

current_datetime = datetime.now()

URL1 = 'https://www.pershingsquareholdings.com/company-reports/letters-to-shareholders/'
URL2 = 'https://www.pershingsquareholdings.com/company-reports/other-materials/'

pdf_dict = {}
excel_dict = {}

def download_file(download_url, path):
    response = requests.get(download_url)
    filename = download_url.split('/')[-1]
    file = open(path + '/' + filename, 'wb')
    file.write(response.content)
    file.close()
    print "Saved file: {}".format(filename)

def generate_metadata(download_url, path, section):
    original_filename = download_url.split('/')[-1]
    filename = '.metadata.'.join((download_url.split('/')[-1]).rsplit('.', 1))
    if section == 'Other Materials':
        filename = filename.replace('xlsx', 'csv')
    elif section == 'Letters to Shareholders':
        filename = filename.replace('pdf', 'csv')
    pub_date = ''.join(download_url.split('/')[4:6])
    with open(path + '/' + filename, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Section', 'Publication Date', 'Title', 'URL'])
        spamwriter.writerow([section, pub_date, original_filename, download_url])
    print 'Generated metadata for: {}'.format(original_filename)

def parse_web_page(URL):
    print 'Scraping URL: {}'.format(URL)
    driver = webdriver.PhantomJS()
    driver.get(URL)
    driver.find_element_by_class_name('close-terms').click()
    dates = driver.find_elements_by_css_selector('div.wpb_column.vc_column_container.vc_col-sm-3 > div > div > div > div > p')
    links = driver.find_elements_by_css_selector('div.wpb_column.vc_column_container.vc_col-sm-9 > div > div > div > div > p > a')
    data = {}
    for date, link in zip(dates, links):
        raw_date = date.text 
        raw_link = link.get_attribute("href")
        data[str(raw_date)] = str(raw_link)
    driver.close()
    print 'Returning scraped data from {}.'.format(URL)
    return data
                              
def main():
    print 'Starting webscraping script\n' + '-' * 150
    #Initializing parser
    parser = argparse.ArgumentParser(description='Webscraper for https://www.pershingsquareholdings.com')
    parser.add_argument('-s', "--start_date", help="Expected format: YYYYMMDD (Inclusive)", 
                        required=True, type=valid_date)
    parser.add_argument('-e', "--end_date", help="Expected format: YYYYMMDD (Inclusive)", 
                        required=True, type=valid_date)
    parser.add_argument('-o', "--output_folder", \
                        help='Directory where output needs to be stored', 
                        required=True)
    
    args = parser.parse_args()

    # Creating PDF directories
    pdf_path = args.output_folder + '/{}/{:02d}/{:02d}/{}'.format(
           current_datetime.year, current_datetime.month, current_datetime.day, current_datetime.strftime('%H%M%S')) + '/letters-to-shareholders'
    os.makedirs(pdf_path)
    
    # Scraping date and href from URL1
    pdf_data = parse_web_page(URL1)
    for date, link in pdf_data.items():
        if check_date(date) >= args.start_date and check_date(date) <= args.end_date:
            pdf_dict[date] = link

    # Saving files to the specified path
    for link in pdf_dict.values():
        download_file(link, pdf_path)
        extract_text(link, pdf_path)
        generate_metadata(link, pdf_path, 'Letters to Shareholders')

    if not pdf_dict:
        print 'No PDF to recover between these timeframes.'
    else:
        print 'Saved {} PDF files.'.format(len(pdf_dict))

    print '-' * 150
    # Creating Excel directories
    excel_path = args.output_folder + '/{}/{:02d}/{:02d}/{}'.format(
           current_datetime.year, current_datetime.month, current_datetime.day, current_datetime.strftime('%H%M%S')) + '/other-materials'
    os.makedirs(excel_path)

    # Scraping date and href from URL2
    excel_data = parse_web_page(URL2)
    for date, link in excel_data.items():
        if check_date(date) >= args.start_date and check_date(date) <= args.end_date:
            excel_dict[date] = link
    
    # Saving files to the specified path
    for link in excel_dict.values():
        download_file(link, excel_path)
        generate_metadata(link, excel_path, 'Other Materials')    

    if not excel_dict:
        print 'No Excel to recover between these timeframes.'
    else:
        print 'Saved {} Excel files.'.format(len(excel_dict))

if __name__ == "__main__":
    main()